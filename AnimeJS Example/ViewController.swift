//
//  ViewController.swift
//  AnimeJS Example
//
//  Created by Christopher Head on 8/8/17.
//  Copyright © 2017 chead. All rights reserved.
//

import UIKit
import JavaScriptCore

class ViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet private var fireworksWebView:     UIWebView!
    @IBOutlet private var particleNumberSlider: UISlider!
              private var jsContext:            JSContext!

    override func viewDidLoad() {
        let indexURL = Bundle.main.url(forResource:   "AnimeJS/documentation/examples/anime-canvas-fireworks",
                                       withExtension: "html")!

        self.fireworksWebView.scrollView.isScrollEnabled = false
        self.fireworksWebView.loadRequest(URLRequest(url: indexURL))
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.jsContext = (fireworksWebView.value(forKeyPath: "documentView.webView.mainFrame.javaScriptContext") as! JSContext)

        let numberOfParticles = self.jsContext.evaluateScript("fireworks.numberOfParticles()")!.toInt32()

        self.particleNumberSlider.maximumValue = Float(numberOfParticles) * 2.0
        self.particleNumberSlider.value        = self.particleNumberSlider.maximumValue / 2.0
    }

    @IBAction func particleNumberSliderValueChanged(sender: UISlider){
        let script = String(format: "fireworks.setNumberOfParticles(%d)", Int(sender.value))

        jsContext.evaluateScript(script)
    }
}
